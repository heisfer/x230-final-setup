#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '
PS1="\\[\e[0;33m\\]\u\\[\e[0;39m\\]:\\[\e[0;34m\\]\w \\[\e[0;39m\\]> "
